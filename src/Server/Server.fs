module Server

open System.IO
open System.Threading.Tasks

open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.V2
open Giraffe
open Shared

open Microsoft.Azure.WebJobs
open Microsoft.Azure.WebJobs.Extensions.Http
open Microsoft.Extensions.Logging

open Fable.Remoting.Server
open Fable.Remoting.Giraffe

open FSharp.Compiler.SourceCodeServices
open FSharp.Compiler.Ast
open Thoth.Json.Net

let publicPath = Path.GetFullPath "../Client/public"
let port = 8085us

let additionaRefs =
    [|
        "--simpleresolution"
        "--noframework"
        //"-r:mscorlib.dll"
        "-r:bin/System.Collections.dll"
        "-r:bin/System.Core.dll"
        "-r:bin/System.Data.dll"
        "-r:bin/System.dll"
        "-r:bin/System.Drawing.dll"
        "-r:bin/System.IO.dll"
        "-r:bin/System.Linq.dll"
        "-r:bin/System.Linq.Expressions.dll"
        "-r:bin/System.Net.Requests.dll"
        "-r:bin/System.Numerics.dll"
        "-r:bin/System.Reflection.dll"
        "-r:bin/System.Runtime.dll"
        "-r:bin/System.Runtime.Numerics.dll"
        "-r:bin/System.Runtime.Remoting.dll"
        "-r:bin/System.Runtime.Serialization.Formatters.Soap.dll"
        "-r:bin/System.Threading.dll"
        "-r:bin/System.Threading.Tasks.dll"
        "-r:bin/System.Web.dll"
        "-r:bin/System.Web.Services.dll"
        "-r:bin/System.Windows.Forms.dll"
        "-r:bin/System.Xml.dll"
    |]

module Result =
    let attempt f =
        try
            Result.Ok <| f()
        with e -> Error e

module Reflection =
    open FSharp.Reflection
    let mapRecordToType<'t> (x:obj) =
        let values = FSharpValue.GetRecordFields x
        FSharpValue.MakeRecord(typeof<'t>, values) :?> 't

let init() : Task<Model> = task { return Model.Default }

let sharedChecker = lazy(FSharpChecker.Create(keepAssemblyContents = true))

let getProjectOptionsFromScript file source defines (checker : FSharpChecker) = async {
    let otherFlags =
        defines
        |> Array.map (fun d -> sprintf "-d:%s" d)
        |> Array.append additionaRefs

    let! (opts, _) = checker.GetProjectOptionsFromScript(file, source, otherFlags = otherFlags, assumeDotNetFramework = true)
    return opts
}

let parse ({ SourceCode = source; Defines = defines; FileName = fileName }) =
    let fileName = if System.String.IsNullOrWhiteSpace fileName then "tmp.fsx" else fileName
    // create ISourceText
    let sourceText = FSharp.Compiler.Text.SourceText.ofString(source)
    // Create an interactive checker instance (ignore notifications)
    let checker = sharedChecker.Value
    // Get compiler options for a single script file
    let checkOptions =
        checker
        |> getProjectOptionsFromScript fileName sourceText defines
        |> (Async.RunSynchronously >> checker.GetParsingOptionsFromProjectOptions >> fst)
    // Run the first phase (untyped parsing) of the compiler
    let untypedRes = checker.ParseFile(fileName, sourceText, checkOptions) |> Async.RunSynchronously
    if untypedRes.ParseHadErrors then
        let errors =
            untypedRes.Errors
            |> Array.filter (fun e -> e.Severity = FSharpErrorSeverity.Error)
        if not <| Array.isEmpty errors then
            failwithf "Parsing failed with errors: %A\nAnd options: %A" errors checkOptions

    match untypedRes.ParseTree with
    | Some tree ->
        tree
    | _ -> failwithf "Parsing failed. Please select a complete code fragment to format."


let typeCheck ({ SourceCode = source; Defines = defines}) =
    let fileName =  "tmp.fsx"
    let sourceText = FSharp.Compiler.Text.SourceText.ofString(source)
    let checker = sharedChecker.Value
    let options =
        checker
        |> getProjectOptionsFromScript fileName sourceText defines
        |> Async.RunSynchronously
    let parseRes, typedRes = checker.ParseAndCheckFileInProject(fileName, 1, sourceText, options) |> Async.RunSynchronously
    match typedRes with
    | FSharpCheckFileAnswer.Aborted ->
        failwithf "Type checking aborted. With Parse errors:\n%A\n And with options: \n%A" parseRes.Errors options
    | FSharpCheckFileAnswer.Succeeded res ->
        match res.ImplementationFile with
        | None -> failwith (sprintf "%A" res.Errors)
        | Some fc -> fc.Declarations

let private rangeEncoder (range: FSharp.Compiler.Range.range) =
    Encode.object [ "StartLine", Encode.int range.StartLine
                    "StartCol", Encode.int range.StartColumn
                    "EndLine", Encode.int range.StartLine
                    "EndCol", Encode.int range.EndColumn ]

let private idEncoder (id: Fantomas.AstTransformer.Id) =
    Encode.object [ "Ident", Encode.string id.Ident
                    "Range", Encode.option rangeEncoder id.Range ]

let private encodeKeyValue (k,v: obj) =
    let (|IsList|_|) (candidate : obj) =
        let t = candidate.GetType()
        if t.IsGenericType && t.GetGenericTypeDefinition() = typedefof<list<_>>
        then Some (candidate :?> System.Collections.IEnumerable)
        else None
    let rec encodeValue (v: obj) =
        match v with
        | null -> Encode.nil
        | :? bool as b -> Encode.bool b
        | :? string as s -> Encode.string s
        | :? int as i -> Encode.int i
        | :? uint16 as ui -> Encode.uint16 ui
        | :? FSharp.Compiler.Range.range as r -> rangeEncoder r
        | :? Fantomas.AstTransformer.Id as id -> idEncoder id
        | :? FSharp.Compiler.Ast.SynModuleOrNamespaceKind as kind ->
            match kind with
            | FSharp.Compiler.Ast.SynModuleOrNamespaceKind.AnonModule -> Encode.string "AnonModule"
            | FSharp.Compiler.Ast.SynModuleOrNamespaceKind.DeclaredNamespace -> Encode.string "DeclaredNamespace"
            | FSharp.Compiler.Ast.SynModuleOrNamespaceKind.GlobalNamespace -> Encode.string "GlobalNamespace"
            | FSharp.Compiler.Ast.SynModuleOrNamespaceKind.NamedModule -> Encode.string "NamedModule"
        | :? ref<bool> as r -> encodeValue r.Value
        | :? option<FSharp.Compiler.Range.range> as o -> Encode.option encodeValue o
        | IsList l -> l |> Seq.cast |> Seq.toList |> List.map encodeValue |> Encode.list
        | meh ->
            printfn "unsupported typeof %A" (meh.GetType())
            Encode.nil
    encodeValue v |> fun ev -> (k,ev)

let rec private nodeEncoder (node: Fantomas.AstTransformer.Node) =
    let properties =
        Map.toList node.Properties
        |> List.map encodeKeyValue
        |> Map.ofList

    Encode.object [
        "Type", Encode.string node.Type
        "Range", Encode.option rangeEncoder node.Range
        "Properties", Encode.dict properties
        "Childs", Encode.list (List.map nodeEncoder node.Childs)
    ]

let parseTask input =
    task {
        return
            if input.SourceCode.Length > Const.sourceSizeLimit then Error "Source size limit exceeded." else
                fun () ->
                    let ast = parse input
                    let node =
                        match ast with
                        | ParsedInput.ImplFile (ParsedImplFileInput.ParsedImplFileInput(_, _, _, _, hds, mns, _)) ->
                            Fantomas.AstTransformer.astToNode hds mns

                        | ParsedInput.SigFile (ParsedSigFileInput.ParsedSigFileInput(_, _, _ , _, mns)) ->
                            Fantomas.AstTransformer.sigAstToNode mns
                        |> nodeEncoder
                        |> Thoth.Json.Net.Encode.toString 2
                    {NodeJson = node; String = sprintf "%A" ast}
                |> Result.attempt
                |> Result.mapError (fun exn -> sprintf "Exception: %s Stack Trace: %s" exn.Message exn.StackTrace) }

let typeCheckTask (({ SourceCode = x; Defines = _ }) as input) =
    task {
        return
            if x.Length > Const.sourceSizeLimit then Error "Source size limit exceeded." else
                Result.attempt <| fun () ->
                    let tast = typeCheck input
                    let node =
                        TastTransformer.tastToNode tast
                        |> nodeEncoder
                        |> Thoth.Json.Net.Encode.toString 2
                    {NodeJson = node; String = sprintf "%A" tast}
                |> Result.mapError (fun exn -> sprintf "Exception: %s Stack Trace: %s" exn.Message exn.StackTrace) }

let getFscVersion () =
    let assembly = typeof<FSharp.Compiler.SourceCodeServices.FSharpChecker>.Assembly
    let version = assembly.GetName().Version
    sprintf "%i.%i.%i" version.Major version.Minor version.Revision
    // typeCheckTask """let x = System.IO.Directory.EnumerateFiles (System.IO.Path.Combine(System.Environment.CurrentDirectory, "bin")) |> String.concat ", " in x"""
    // |> Async.AwaitTask |> Async.RunSynchronously |> sprintf "%A"
    |> fun v -> async { return v }

let modelApi =
    { init = init >> Async.AwaitTask
      parse = parseTask >> Async.AwaitTask
      version = getFscVersion
      typeCheck = typeCheckTask >> Async.AwaitTask }

let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue modelApi
    |> Remoting.buildHttpHandler

// code from https://github.com/giraffe-fsharp/Giraffe.AzureFunctions
[<FunctionName "Giraffe">]
let run ([<HttpTrigger (AuthorizationLevel.Anonymous, Route = "{*any}")>] req : HttpRequest, context : ExecutionContext, _ : ILogger) =
  let hostingEnvironment = req.HttpContext.GetHostingEnvironment()
  hostingEnvironment.ContentRootPath <- context.FunctionAppDirectory
  let func = Some >> Task.FromResult
  task {
    let! _ = webApp func req.HttpContext
    req.HttpContext.Response.Body.Flush() //workaround https://github.com/giraffe-fsharp/Giraffe.AzureFunctions/issues/1
    } :> Task